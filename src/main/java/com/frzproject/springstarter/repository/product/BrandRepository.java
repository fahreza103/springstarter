package com.frzproject.springstarter.repository.product;

import com.frzproject.springpersistence.repository.BasicRepository;
import com.frzproject.springstarter.model.product.Brand;

public interface BrandRepository <T extends Brand> extends BasicRepository<T>{

	public Brand findByName(String name);
}
