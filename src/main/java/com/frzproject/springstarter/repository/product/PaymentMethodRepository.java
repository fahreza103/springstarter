package com.frzproject.springstarter.repository.product;

import java.util.Set;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;

import com.frzproject.springpersistence.repository.BasicRepository;
import com.frzproject.springstarter.model.product.PaymentMethod;




public interface PaymentMethodRepository <T extends PaymentMethod> extends BasicRepository<T>{

	@Cacheable(value = "findAllCache")	
	@Query("select a from PaymentMethod a")
	public Set<PaymentMethod> findAllCached();
}
