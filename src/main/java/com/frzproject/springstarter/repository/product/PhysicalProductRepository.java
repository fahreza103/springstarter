package com.frzproject.springstarter.repository.product;

import com.frzproject.springpersistence.repository.BasicRepository;
import com.frzproject.springstarter.model.product.PhysicalProduct;

public interface PhysicalProductRepository<T extends PhysicalProduct> extends BasicRepository<T> {

}
