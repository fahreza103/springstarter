package com.frzproject.springstarter.repository.product;

import com.frzproject.springpersistence.repository.BasicRepository;
import com.frzproject.springstarter.model.product.Product;

public interface ProductRepository <T extends Product> extends BasicRepository<T>{

}
