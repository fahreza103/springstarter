package com.frzproject.springstarter.repository.product;

import com.frzproject.springpersistence.repository.BasicRepository;
import com.frzproject.springstarter.model.product.StockType;

public interface StockTypeRepository<T extends StockType> extends BasicRepository<T> {

}
