package com.frzproject.springstarter.repository.seeder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;


@Component
public class DatabaseSeeder {
	
	@Value("${database.seed.enabled}")
	public boolean enableSeed;

	//@Autowired
	//private AppUserSeeder appUserSeeder;
	
	@Autowired
	private ProductSeeder productSeeder;
	

	
    @EventListener
    public void seed(ContextRefreshedEvent event) {
    	if(enableSeed) {
        	productSeeder.seedProduct();
        	//appUserSeeder.seedUser();
    	}
    }
}
