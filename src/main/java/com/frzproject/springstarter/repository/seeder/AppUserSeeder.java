//package com.frzproject.springstarter.repository.seeder;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Component;
//
//import com.frzproject.springstarter.model.user.AppUser;
//import com.frzproject.springstarter.model.user.UserRole;
//import com.frzproject.springstarter.repository.user.AppUserRepository;
//import com.frzproject.springstarter.repository.user.UserRoleRepository;
//
//
//
//@Component
//public class AppUserSeeder {
//
//	@Autowired
//	private AppUserRepository<AppUser> userRepository;
//	
//	@Autowired
//	private UserRoleRepository<UserRole> userRoleRepository;
//
//	@Autowired
//	private BCryptPasswordEncoder encoder;
//	
//	private Logger logger = LoggerFactory.getLogger(AppUserSeeder.class);
//	
//	public List<AppUser> seedUser() {
//		logger.info("Seed Role Table");
//		List<UserRole> userRoleList = userRoleRepository.findAll();
//		List<AppUser> userList = new ArrayList<AppUser>();
//		if(userRoleList.size() == 0) {
//			userRoleList = seedRole();
//			
//			logger.info("Seed User Table");
//			userList = new ArrayList<AppUser>();
//			userList.add(new AppUser("fahreza",encoder.encode("admin"),"fahreza.tamara@gmail.com",userRoleList.get(0)));
//			userList.add(new AppUser("budi",encoder.encode("12345"),"budi@gmail.com",userRoleList.get(1)));
//			userList.add(new AppUser("eko",encoder.encode("54321"),"eko@gmail.com",userRoleList.get(1)));
//			userRepository.saveAll(userList);
//			
//			logger.info("Success Seeding User Data, size :"+userList.size());
//		}
//		return userList;
//	}
//	
//	private  List<UserRole> seedRole() {
//		List<UserRole> userRoleList = new ArrayList<UserRole>();
//
//
//		
//		UserRole adminRole =  new UserRole("ADMIN","Admin Applikasi");
//		userRoleList.add(adminRole);
//		
//		UserRole operatorRole = new UserRole("OPERATOR","Operator");
//		userRoleList.add(operatorRole);
//		
//
//		userRoleRepository.saveAll(userRoleList);
//		logger.info("Success Seeding User Role Data, size :"+userRoleList.size());
//		return userRoleList;
//	}
//	
//
//	
//
//	
//	@Bean
//	public BCryptPasswordEncoder passwordEncoder() {
//	    return new BCryptPasswordEncoder();
//	}
//}
