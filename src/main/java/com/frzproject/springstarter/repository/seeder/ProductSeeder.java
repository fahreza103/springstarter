package com.frzproject.springstarter.repository.seeder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.frzproject.springstarter.model.product.Brand;
import com.frzproject.springstarter.model.product.PaymentMethod;
import com.frzproject.springstarter.model.product.PhysicalProduct;
import com.frzproject.springstarter.model.product.Product;
import com.frzproject.springstarter.model.product.ServiceProduct;
import com.frzproject.springstarter.model.product.StockType;
import com.frzproject.springstarter.repository.product.BrandRepository;
import com.frzproject.springstarter.repository.product.PaymentMethodRepository;
import com.frzproject.springstarter.repository.product.ProductRepository;
import com.frzproject.springstarter.repository.product.StockTypeRepository;


@Component
public class ProductSeeder {
	
	private Logger logger = LoggerFactory.getLogger(ProductSeeder.class);
	@Autowired
	private BrandRepository<Brand> brandRepository;
	@Autowired
	private ProductRepository<Product> productRepository;
	@Autowired
	private StockTypeRepository<StockType> stockTypeRepository;
	@Autowired
	private PaymentMethodRepository<PaymentMethod> paymentMethodRepository;
	
	public void seedProduct() {
		logger.info("Seed stock type");
		
		Brand brand = brandRepository.findByName("MIMAKI");
		if(brand == null) {
			List<StockType> stockTypeList = new ArrayList<StockType>();
			stockTypeList.add(new StockType("SATUAN","Tipe kuantitas per satuan","pc"));
			stockTypeList.add(new StockType("METERAN","Tipe kuantitas per meter","m"));		
			stockTypeRepository.saveAll(stockTypeList);
			logger.info("Success, rows inserted :"+stockTypeList.size());
			
			logger.info("Seed Brand");
			List<Brand> brandList = new ArrayList<Brand>();
			brandList.add(new Brand("MIMAKI", "Made in Japan, specialize in digital printing machine"));
			brandList.add(new Brand("ROLAND", "Made in Japan, specialize in digital printing machine"));
			brandList.add(new Brand("ORAFOL", "Made In Germany, Specialize in sticker"));
			brandList.add(new Brand("RITRAMA", "Made In Italy, Specialize in sticker"));
			brandList.add(new Brand("3M", "Made In USA, Specialize in sticker"));
			brandRepository.saveAll(brandList);
			logger.info("Success, rows inserted :"+brandList.size());
			
			logger.info("Seed Product");
			List<Product> productList = new ArrayList<Product>();
			PhysicalProduct orajet = new PhysicalProduct("ORAJET","Orajet For Printing",true);
			orajet.setStock(500d).setStockType(stockTypeList.get(1)).setBrand(brandList.get(2)).setSellPrice(new BigDecimal(75000)).setBuyPrice(new BigDecimal(30000));
			productList.add(orajet);
			
			PhysicalProduct oracal651BlackDoff = new PhysicalProduct("ORACAL 651 Black Doff","For wrapping",true);
			oracal651BlackDoff.setStock(500d).setStockType(stockTypeList.get(1)).setBrand(brandList.get(2)).setSellPrice(new BigDecimal(55000)).setBuyPrice(new BigDecimal(20000));
			productList.add(oracal651BlackDoff);
			
			PhysicalProduct oracal651Glossy = new PhysicalProduct("ORACAL 651 Black Glossy","For wrapping",true);
			oracal651Glossy.setStock(500d).setStockType(stockTypeList.get(1)).setBrand(brandList.get(2)).setSellPrice(new BigDecimal(55000)).setBuyPrice(new BigDecimal(20000));
			productList.add(oracal651Glossy);
			
			PhysicalProduct ritramaPrinting = new PhysicalProduct("Ritrama Vinyl","For Printing",true);
			ritramaPrinting.setStock(500d).setStockType(stockTypeList.get(1)).setBrand(brandList.get(3)).setSellPrice(new BigDecimal(65000)).setBuyPrice(new BigDecimal(25000));
			productList.add(ritramaPrinting);
	
			ServiceProduct jasaPasangSticker = new ServiceProduct("Jasa Pasang Sticker","Pemasangan sticker 1 body");
			productList.add(jasaPasangSticker);
			ServiceProduct jasaDesign = new ServiceProduct("Jasa Design","Design 1 decal full");
			productList.add(jasaDesign);
			ServiceProduct jasaServiceAC = new ServiceProduct("Jasa Service AC","Service AC");
			jasaServiceAC.setBuyPrice(new BigDecimal(100000));
			jasaServiceAC.setIsSoldable(false);
			productList.add(jasaServiceAC);
			productRepository.saveAll(productList);
			logger.info("Success, rows inserted :"+productList.size());
			
			
			logger.info("Seed Payment Method");
			List<PaymentMethod> paymentMethodList = new ArrayList<PaymentMethod>();
			paymentMethodList.add(new PaymentMethod("CASH", "With Physical Money Directly"));
			paymentMethodList.add(new PaymentMethod("BANK_TRANSFER", "Bank Transfer, any bank included"));
			paymentMethodList.add(new PaymentMethod("CREDIT_CARD", "Credit card payment, any bank included"));
			paymentMethodList.add(new PaymentMethod("GOPAY", "GOPAY Payment by GOJEK"));
			paymentMethodList.add(new PaymentMethod("OVO", "OVO Payment"));
			paymentMethodRepository.saveAll(paymentMethodList);
			logger.info("Success, rows inserted :"+paymentMethodList.size());
		} else {
			logger.info("Data already exist, seed not required");
		}
	}

}
