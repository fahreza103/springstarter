package com.frzproject.springstarter.repository.user;


import com.frzproject.springpersistence.repository.BasicRepository;
import com.frzproject.springstarter.model.user.AppUser;
import com.frzproject.springstarter.model.user.UserRole;



public interface UserRoleRepository<T extends UserRole> extends BasicRepository<T> {

	public AppUser findByName(String name);
	
}
