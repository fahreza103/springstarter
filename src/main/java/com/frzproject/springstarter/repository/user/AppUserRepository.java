package com.frzproject.springstarter.repository.user;


import com.frzproject.springpersistence.repository.BasicRepository;
import com.frzproject.springstarter.model.user.AppUser;



public interface AppUserRepository<T extends AppUser> extends BasicRepository<T> {

	public AppUser findByUsername(String username);
	
	public AppUser findByEmail(String email);
}
