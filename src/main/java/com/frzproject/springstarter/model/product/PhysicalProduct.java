package com.frzproject.springstarter.model.product;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.frzproject.springstarter.constant.ProductType;


@Entity
@DiscriminatorValue(ProductType.Values.PHYSICAL)
public class PhysicalProduct extends Product {

	private static final long serialVersionUID = 7982735444207077367L;
	
	public PhysicalProduct() {super();}
	
	public PhysicalProduct(String name, String description, Boolean isSoldable) {
		super(name, description, isSoldable);
	}
	
	@Column(name = "stock", nullable = false)
	@ColumnDefault("0")
	private Double stock = 0d;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stock_type_id", foreignKey = @ForeignKey(name = "product_stock_type_id_fk"))
	@JsonBackReference
	private StockType stockType;

	public Double getStock() {
		return stock;
	}

	public PhysicalProduct setStock(Double stock) {
		this.stock = stock;
		return this;
	}

	public StockType getStockType() {
		return stockType;
	}

	public PhysicalProduct setStockType(StockType stockType) {
		this.stockType = stockType;
		return this;
	}
	
	

}
