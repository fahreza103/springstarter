package com.frzproject.springstarter.model.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.frzproject.springpersistence.entity.GenericWithIdentifier;

@Entity
@Table(name="BRANDS")
public class Brand extends GenericWithIdentifier {

	private static final long serialVersionUID = -2489519411830847808L;
	
	@OneToMany(mappedBy = "brand", cascade = CascadeType.ALL)
	@JsonManagedReference("brand")
	private Set<Product> products;
	
	public Brand() {
		super();
	}
	
	public Brand(String name, String description) {
		super(name, description);
	}

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}
	
	
	@Override
	public String toString() {
	  return ToStringBuilder.reflectionToString(this);
	}

}
