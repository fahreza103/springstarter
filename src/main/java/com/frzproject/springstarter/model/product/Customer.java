package com.frzproject.springstarter.model.product;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.frzproject.springpersistence.entity.Generic;

@Entity
@Table(name="CUSTOMERS")
public class Customer extends Generic {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3929754827877813909L;

	@NotNull(message = "The name must not be null")
	@Column(name = "customer_name", nullable = false,unique=true,length = 255)
	public String customerName;
	
	@Column(name = "customer_email", length=50)
	@Email
	public String customerEmail;
	
	@Column(name = "customer_contact", length=50)
	public String customerContact;

	@OneToMany(fetch = FetchType.LAZY,mappedBy = "customer")
	@JsonManagedReference("customer")
	private Set<Order> orders;
	
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerContact() {
		return customerContact;
	}

	public void setCustomerContact(String customerContact) {
		this.customerContact = customerContact;
	}
	
	

}
