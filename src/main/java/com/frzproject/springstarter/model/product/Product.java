package com.frzproject.springstarter.model.product;

import java.math.BigDecimal;


import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.frzproject.springpersistence.entity.GenericWithIdentifier;
import com.frzproject.springstarter.constant.ProductType;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;


@Entity
@Table(name="PRODUCTS")
@DiscriminatorColumn(name="product_type", discriminatorType=DiscriminatorType.STRING)
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "productType")
@JsonSubTypes({
	@Type(value=PhysicalProduct.class,name=ProductType.Values.PHYSICAL),
	@Type(value=ServiceProduct.class,name=ProductType.Values.SERVICE)
})
public class Product extends GenericWithIdentifier {
	
	private static final long serialVersionUID = -8866707792225815832L;

	public Product() {
		super();
	}
	
	public Product(String name, String description, Boolean isSoldable) {
		super(name, description);
		this.isSoldable = isSoldable;
	}
	
	@Column(name = "product_type",nullable=false,insertable=false,updatable=false)
	@Enumerated(EnumType.STRING)
	private ProductType productType;
	
	@Column(name = "is_soldable",nullable=false)
	private Boolean isSoldable = false;
	
	@Column(name = "buy_price",nullable=false)
	private BigDecimal buyPrice = new BigDecimal(0);
	
	@Column(name = "sell_price",nullable=false)
	private BigDecimal sellPrice = new BigDecimal(0);
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "brand_id", foreignKey = @ForeignKey(name = "product_brand_id_fk"))
	@JsonBackReference("brand")
	private Brand brand;
	
	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public Brand getBrand() {
		return brand;
	}

	public Product setBrand(Brand brand) {
		this.brand = brand;
		return this;
	}
	
	public Boolean getIsSoldable() {
		return isSoldable;
	}

	public Product setIsSoldable(Boolean isSoldable) {
		this.isSoldable = isSoldable;
		return this;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public Product setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
		return this;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public Product setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
		return this;
	}

	@Override
	public String toString() {
	  return ToStringBuilder.reflectionToString(this);
	}
}
