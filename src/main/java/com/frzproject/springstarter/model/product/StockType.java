package com.frzproject.springstarter.model.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.frzproject.springpersistence.entity.GenericWithIdentifier;

@Entity
@Table(name="STOCK_TYPES")
public class StockType extends GenericWithIdentifier {

	private static final long serialVersionUID = -3259768215575151788L;
	
	public StockType() {super();}
	
	public StockType(String name, String description, String unit) {
		super(name,description);
		this.unit = unit;
	}
	
	@Column(name = "unit",nullable=false)
	private String unit;
	
	@OneToMany(mappedBy = "stockType", cascade = CascadeType.ALL)
	@JsonManagedReference
	private Set<PhysicalProduct> physicalProducts;

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Set<PhysicalProduct> getPhysicalProducts() {
		return physicalProducts;
	}

	public void setPhysicalProducts(Set<PhysicalProduct> physicalProducts) {
		this.physicalProducts = physicalProducts;
	}
	
	
	

}
