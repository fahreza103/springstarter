package com.frzproject.springstarter.model.product;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.frzproject.springstarter.constant.ProductType;



@Entity
@DiscriminatorValue(ProductType.Values.SERVICE)
public class ServiceProduct extends Product {
	
	public ServiceProduct() {super();}
	
	public ServiceProduct(String name, String description) {
		super(name, description, true);
	}
	
	@Column(name="service_duration")
	public Integer serviceDuration;
	
	

	public Integer getServiceDuration() {
		return serviceDuration;
	}

	public void setServiceDuration(Integer serviceDuration) {
		this.serviceDuration = serviceDuration;
	}

	private static final long serialVersionUID = -8356281175263795804L;
	

}
