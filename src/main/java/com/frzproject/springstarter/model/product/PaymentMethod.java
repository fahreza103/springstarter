package com.frzproject.springstarter.model.product;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.frzproject.springpersistence.entity.GenericWithIdentifier;


@Entity
@Table(name="PAYMENT_METHODS")
public class PaymentMethod extends GenericWithIdentifier {

	private static final long serialVersionUID = 7284242441032221655L;
	
	public PaymentMethod(String name, String description) {
		super(name,description);
	}

	
}
