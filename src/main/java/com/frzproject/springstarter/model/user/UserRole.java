package com.frzproject.springstarter.model.user;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.frzproject.springpersistence.entity.GenericWithIdentifier;

@Entity
@Table(name="USER_ROLES")
public class UserRole  extends GenericWithIdentifier {

	private static final long serialVersionUID = -6108780846294631441L;
	
	public UserRole() {}
	
	public UserRole(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	
	@OneToMany(fetch = FetchType.LAZY,mappedBy = "role", cascade = CascadeType.ALL)
	@JsonBackReference
	private Set<AppUser> appUsers;
	

	public Set<AppUser> getAppUsers() {
		return appUsers;
	}


	public void setAppUsers(Set<AppUser> appUsers) {
		this.appUsers = appUsers;
	}

	@Override
	public String toString() {
	  return ToStringBuilder.reflectionToString(this);
	}
}
