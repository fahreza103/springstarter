package com.frzproject.springstarter.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.frzproject.springpersistence.entity.Generic;


@Entity
@Table(name="USERS")
public class AppUser  extends Generic {

	private static final long serialVersionUID = 2062327631757923738L;
	
	@Column(name = "username", nullable = false,unique=true, length = 100)
	private String username;
	@Column(name = "password", nullable = false, length = 255)
	private String password;
	@Column(name = "email", nullable = false,unique=true, length = 100)
	private String email;
	@Column(name = "phone",length=12)
	private String telephoneNumber;
	@ManyToOne
	@JoinColumn(name = "role_id", foreignKey = @ForeignKey(name = "user_role_id_fk"),nullable=false)
	private UserRole role;
	
	
	public AppUser() {}
	
	public AppUser(String username, String password, String email, UserRole userRole) {
		this.username = username;
		this.password = password;
		this.email    = email;
		this.role = userRole;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}



	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	@Override
	public String toString() {
	  return ToStringBuilder.reflectionToString(this);
	}
	
}
