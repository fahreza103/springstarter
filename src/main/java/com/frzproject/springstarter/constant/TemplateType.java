package com.frzproject.springstarter.constant;

public enum TemplateType {

	EMAIL_TEMPLATE, SMS_TEMPLATE
}
