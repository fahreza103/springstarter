package com.frzproject.springstarter.constant;

public enum CustomerType {
	INDIVIDUAL, ORGANIZATION
}
