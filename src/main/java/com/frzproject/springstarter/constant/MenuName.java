package com.frzproject.springstarter.constant;

public class MenuName {

	// ========== PARENT =======================
	public static final String MENU_MASTER = "MENU_MASTER";
	public static final String MENU_CUSTOMER = "MENU_CUSTOMER";
	public static final String MENU_ADMINISTRATION = "MENU_ADMINISTRATION";
	public static final String MENU_TRANSACTION = "MENU_TRANSACTION";
	public static final String MENU_REPORT = "MENU_REPORT";
	
	// ========== CHILDREN =====================
	public static final String MENU_MASTER_PRODUCT = "MENU_MASTER_PRODUCT";
	public static final String MENU_MASTER_CUSTOMER = "MENU_MASTER_CUSTOMER";
	public static final String MENU_ADMINISTRATION_USER = "MENU_ADM_USER";
	public static final String MENU_ADMINISTRATION_ROLE = "MENU_ADM_ROLE";
	public static final String MENU_ADMINISTRATION_CONFIG = "MENU_ADM_CONFIG";
	public static final String MENU_TRANSACTION_PURCHASE = "MENU_TRN_PURCHASE";
	public static final String MENU_TRANSACTION_ORDER = "MENU_TRN_ORDER";
	
}
