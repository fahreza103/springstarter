package com.frzproject.springstarter.constant;

public enum ProductType {
	PHYSICAL(Values.PHYSICAL),SERVICE(Values.SERVICE);
	
	  ProductType (String val) {
	     // force equality between name of enum instance, and value of constant
	     if (!this.name().equals(val))
	        throw new IllegalArgumentException("Incorrect use of ProductType");
	  }
	
	public static class Values {
	     public static final String PHYSICAL= "PHYSICAL";
	     public static final String SERVICE= "SERVICE";
	}
}
