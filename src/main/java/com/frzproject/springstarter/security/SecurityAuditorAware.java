//package com.frzproject.springstarter.security;
//
//import java.util.Optional;
//
//import org.springframework.data.domain.AuditorAware;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//
//import com.frzproject.springstarter.model.user.AppUser;
//
//
//
//public class SecurityAuditorAware implements AuditorAware<String>{
//
//	@Override
//	public Optional<String> getCurrentAuditor() {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        if (authentication == null || !authentication.isAuthenticated()) {
//            return Optional.empty();
//        }
//        // Default
//        String username = "system";
//        if(authentication.getPrincipal() instanceof AppUser) {
//        	username = ((AppUser) authentication.getPrincipal()).getUsername();
//        } else {
//        	username = authentication.getPrincipal().toString();
//        }
//        return Optional.of(username);
//	}
//
//}
