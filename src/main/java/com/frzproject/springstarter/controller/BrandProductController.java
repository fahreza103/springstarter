package com.frzproject.springstarter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.frzproject.springpersistence.controller.CrudController;
import com.frzproject.springstarter.model.product.Brand;
import com.frzproject.springstarter.repository.product.BrandRepository;

@RestController
@RequestMapping(value = "product")
public class BrandProductController extends CrudController<Brand> {

	@Autowired
	public BrandProductController(BrandRepository<Brand> brandRepository) {
		super(brandRepository);
	}

}
