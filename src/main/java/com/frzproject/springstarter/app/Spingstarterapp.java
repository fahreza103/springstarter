package com.frzproject.springstarter.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("com.frzproject.springstarter")
@EntityScan("com.frzproject.springstarter.model")
@EnableJpaRepositories("com.frzproject.springstarter.repository")
public class Spingstarterapp {
	public static void main(String[] args) {
		SpringApplication.run(Spingstarterapp.class, args);
	}
}
